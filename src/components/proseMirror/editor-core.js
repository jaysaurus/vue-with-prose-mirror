import { DOMParser } from 'prosemirror-model'
import { EditorState } from 'prosemirror-state'
import { EditorView } from 'prosemirror-view'
import { schema } from 'prosemirror-schema-basic'

import { bubbleKeymap } from './menu-bubble-commands.js'

function createEditorState (ed) {
  return EditorState.create({
    doc: DOMParser.fromSchema(schema).parse(ed.doc),
    schema,
    plugins: [
      bubbleKeymap
    ]
  })
}

function createEditorView (ed, querySelector, view = undefined) {
  view = new EditorView(ed.doc, {
    state: ed.state,
    dispatchTransaction (transaction) {
      let newState = view.state.apply(transaction)
      ed.doc = document.querySelector(querySelector)
      view.updateState(newState)
    }
  })
  return view
}

export { createEditorState, createEditorView }
