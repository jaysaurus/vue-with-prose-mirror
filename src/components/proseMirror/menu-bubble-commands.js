import { keymap } from 'prosemirror-keymap'
import { toggleMark } from 'prosemirror-commands'
import { schema } from 'prosemirror-schema-basic'

const _commands = [
  { command: toggleMark(schema.marks.strong),
    name: 'bold',
    type: 'command',
    icon: 'B',
    shortcut: 'Mod-b' },
  { command: toggleMark(schema.marks.em),
    name: 'italic',
    type: 'command',
    icon: 'I',
    shortcut: 'Mod-i' }
]

const _keymap = keymap(
  _commands.reduce((obj, item) => {
    if (item.hasOwnProperty('shortcut')) {
      obj[item.shortcut] = item.command
    }
    return obj
  }, {}))

export { _keymap as bubbleKeymap, _commands as bubbleCommands }
